# Syapse CDA Assignment 
## Candidate: Mahder Teka 
### Date: April 20, 2019

**Genetic Characterization**

To `genetically characterize` the population from the given set of documents, the data was grouped by the **biomarkers** primarily, then by the type of 
mutation they acquired and finally by the gene the mutation occurred in. 


## Code:

```{r, message = FALSE}
library(tidyverse)
library(dplyr)


#read in input files 
biomarkers <- read.csv("biomarkers.csv", header = TRUE)
icds <- read.csv("icds.csv", header = TRUE)
patients <- read.csv("patients.csv", header = TRUE)
therapies <- read.csv("therapies.csv", header = TRUE)

#population genetic characterization by grouping by biomarker, mutation and gene
df <- biomarkers %>% group_by(biomarker, alteration.type, gene) %>% summarize(n())

```
This resulted in a characterized dataset based on `genetic background` i.e. The number of patients that share a biomarkers
as a result of a mutation that occurred in a specific gene. Below is a the first 5 entries of this new categorized dataset: 

Biomarker | Mutation type | gene | Number of patients
----------| --------------|----- | ------------------
KDR Q472H|Substitution|KDR|472
KDR 1416A>T|Substitution|KDR|470
KIT M541L|Substitution|KIT|125
KIT 1621A>C|Substitution|KIT|123
TYMS *447_*452delTTAAAG|Deletion|TYMS|93

**Clinical Implication**

To examine `clinical implication`, one subset of the population was selected. To start the analysis, all the provided datasets were joined together using
the patient ID as an identifier. This enables a comprehensive overview of all the available information regarding the patients, their background, diagnosis and
treatment they may have received. 

The biomarker that was selected at random for this subset is *ERBB2 NC_000017.10:g.A37880981AGCATACGTGATG* with a complex alternation in the ERB2 gene.

## Code:

```{r, message = FALSE}
biomarker_subset <- biomarkers %>% filter(biomarker == "ERBB2 NC_000017.10:g.A37880981AGCATACGTGATG", alteration.type == "Complex")

joins <- left_join(biomarker_subset, icds, by='id') %>% 
  left_join(., patients, by='id') %>%
  left_join(., therapies,  by='id')
```

This new dataset shows that there are `7 patients` that have the biomarker in question. While 3 of these patients have been deceased, 4 of them are still 
alive. For the living patients', all of were diagnosed with a condition that affects their Bronchus & Lung where as the 4th patient was diagnosed with an 
additional condition that affects her breast. The patients in this category are all female. 

Regarding the deceased patients, two of them had a diagnosis that affected their Bronchus & Lung where as the third was had a pancreatic related diagnosis. 
Two of the patients in this category are males and one is female. 

To dig into what the clinical implication of the `ERBB2 NC_000017.10:g.A37880981AGCATACGTGATG` biomarker could be, the shared therapeutic medication between the patients
was analysed. 

* *FLUORINE-18 FLUORODEOXYGLUCOSE* was used to treat three of the seven patients and 2 of them have been deceased. The living patient `PT-56804` is female and 
is diagnosed with a problem in her Bronchus & Lung which is the same case as one of the deceased patients `PT-47761`. However, `PT-56804` is relatively younger,
28 years younger to be exact, so perhaps age is a factor as to how this biomarker affects therapeutic response to the *FLUORINE-18 FLUORODEOXYGLUCOSE* medication. 

* It is also possible that `gender` plays a role in the way this biomarker affects patients as most of the diagnosed patients were females and the only two male 
patients have already been deceased. 

![image](images/plots.png)

**Methodology**

The `methodology` used for this analysis was based on grouping the population by shared similarities regarding the mutations they acquired and looking at how 
that affected their response to therapy. Alongside genetic information, other patient information such as age and gender was also taken into consideration. 

The code that was used has partially been shared throughout this document but a complete script is also available within the directory. 

**Limitations**

* There is not enough `background information` on the patients such as date of diagnosis and duration of therapeutic treatment. 
* There is not enough information of the `medication` the patients were given, such as weather it targets a specific biological pathway. 
* Regarding the `mutations` the patients acquired, there isn't enough information positional information to state where in the gene it occured. 

**Further Exploration**

If additional information could be provided on the `mechanism of action` of the therapeutic drugs, then targeted site or biochemical pathway can be looked at
to see if additional mutations have occurred in that region. 

For the existing `mutations` that have been identified, the position within the DNA that it occured in could provide more insight as to why patients in with 
the same biomarker are responding differently to treatment. 

Data regarding the patients' `race and ethnicity` could also be useful as there have been researches that showed race specific cancer mutations. 

Finally, more information on the patients' diagnosis and treatment plans can be useful information when trying to analyse patients in the same disease category. 


